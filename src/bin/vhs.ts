#!/usr/bin/env node
import {
    readFileSync,
    ensureDir,
    readdirSync,
    removeSync,
    moveSync
} from 'fs-extra';
import { exec } from 'child_process';
import { join } from 'path';
import * as Vorpal from 'vorpal';
import * as ProgressBar from 'progress';
import { VideoImport, VideoEdit } from '../lib';


const chalk = Vorpal().chalk;
const logo = readFileSync(join(__dirname, '../logo.txt'));
const colors = [
    chalk.green,
    chalk.yellow,
    chalk.blue,
    chalk.magenta
];
const cli = new Vorpal();
const welcomeColor = colors[Math.floor(Math.random() * colors.length)];
const escapeArgs = (command: string, args: string): string => command.replace(/=/g, '`=');
const unEscape = (command: string): string => command.replace(/`=/g, '=');
const ROOT = 'VHS$';
const PROJ_DIR = './assets';

const actions = {
    async downloadYouTubeVideo(args) {
        const url  = new URL(unEscape(args.url));
        const downloadBarCfg = {
                complete: '=',
                incomplete: ' ',
                width: 20
            };
        let bar;
        const name = await VideoImport.fetchAsset(url, PROJ_DIR, (chunkLength: number, downloaded: number, total: number) => {
            if (!bar) {
                bar = new ProgressBar('downloading [:bar] :rate/bps :percent :etas', { ...downloadBarCfg, total});
            }
            bar.tick(chunkLength);
        });
        this.log(name);
    },
    async open(args) {
        const video  = join(PROJ_DIR, unEscape(args.name));
        if (process.platform === 'darwin') {
            exec(`open ${video}`);
        } else if (process.platform === 'linux') {
            exec(`xdg-open ${video}`);
        } else {
            this.log('This command cannot be executed on your platform');
        }

    },
    async list() {
        readdirSync(PROJ_DIR).forEach(file => {
            console.log(file);
        });
    },
    async splice(args) {
        const spliceBarCfg = {
            complete: '=',
            incomplete: ' ',
            width: 20,
            total: 100
        };
        let bar = new ProgressBar('processing [:bar] :percent :etas', spliceBarCfg);
        const sgmt = { name: args.dst, time: args.start, duration: args.duration };
        const newAssetName = await VideoEdit.spliceSegment(args.src, sgmt, PROJ_DIR, (progress) => { bar.update(progress.percent); });
        bar.update(1);
        this.log(newAssetName);
    },
    async preview(args) {
        const prvBarCfg = {
            complete: '=',
            incomplete: ' ',
            width: 20,
            total: 100 * (args.names.length + 1)
        };
        let bar = new ProgressBar('processing [:bar] :percent :etas', prvBarCfg);
        let total = 100 * (args.names.length + 1);
        const newAssetName = await VideoEdit.mergeSegments(args.dst, args.names, PROJ_DIR, (progress) => { bar.update(progress.percent / total); });
        bar.update(1);
        this.log(newAssetName);
    },
    async rm(args) {
        args.names.forEach(file => {
            removeSync(join(PROJ_DIR, file));
        });
        readdirSync(PROJ_DIR).forEach(file => {
            console.log(file);
        });
    },
    async mv(args) {
        moveSync(join(PROJ_DIR, args.src), join(PROJ_DIR, args.dst));
        readdirSync(PROJ_DIR).forEach(file => {
            console.log(file);
        });
    },
};

function start() {
    console.log(welcomeColor(chalk.bgBlack(logo.toString())));
    ensureDir(PROJ_DIR);
    cli
        .command('dl <url>', 'Download video content from Youtube')
        .parse(escapeArgs)
        .action(actions.downloadYouTubeVideo);

    cli
        .command('open <name>', 'Open the video file using default player')
        .parse(escapeArgs)
        .action(actions.open);

    cli
        .command('ls', 'List video assets')
        .action(actions.list);

    cli
        .command('prv <dst> [names...]', 'Merges splices into a new video for preview')
        .action(actions.preview);

    cli
        .command('spl <src> <dst> <start> <duration>', 'Splices a source video into a destination video with start and duration in seconds')
        .action(actions.splice);

    cli
        .command('rm [names...]', 'Deletes assets')
        .action(actions.rm);

    cli
        .command('mv <src> <dst>', 'Moves/Renames a file.')
        .action(actions.mv);

    cli
        .delimiter(ROOT)
        .show();
}

start();