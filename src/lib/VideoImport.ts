import { existsSync, createWriteStream } from 'fs-extra';
import { join } from 'path';
const ytdl = require('ytdl-core');

export type progresCb = (chunkLength: number, downloaded: number, total: number) => void;

export class VideoImport {

    public static async getAssetInfo(url: URL) {
        return await ytdl.getInfo(url.href);
    }

    public static async fetchAsset(url: URL, path: string = './', progressCb: progresCb = () => { }): Promise<string> {
        return new Promise<string>((resolve, reject) => {
            let fileName = `${url.searchParams.get('v')}.mp4`;
            if (existsSync(join(path, fileName))) {
                return resolve(fileName);
            }
            const video = ytdl(url.href, { filter: (format: any) => format.container === 'mp4' });
            video.pipe(createWriteStream(join(path, fileName)));
            video.on('progress', progressCb);
            video.on('end', () => {
                resolve(join(path, fileName));
            });
        });
    }
}