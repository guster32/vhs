import * as FFmpeg from 'fluent-ffmpeg';
import { join } from 'path';

export type segment = {
    name: string,
    time: string,
    duration: string
};

export type progresCb = (percent: any) => void;

export class VideoEdit {

    public static async spliceSegment(src: string, segment: segment, folder: string = './', progressCb: progresCb = () => { }) {
        let spliceName = `${segment.name}.mp4`;
        return new Promise<string>((resolve, reject) => {
            console.log(`Splicing:${spliceName} with StartTime:${segment.time} Duration:${segment.duration.toString()}`);
            FFmpeg({ source: `${join(folder, src)}`, timeout: 0})
                .setStartTime(segment.time)
                .setDuration(segment.duration)
                .output(join(folder, spliceName))
                .on('progress', progressCb)
                .on('end', function(err: Error) {
                    if (!err) {
                        return resolve(spliceName);
                    }
                    reject(err);
                })
                .on('error', function(err: Error) {
                    reject(err);
                }).run();
        });
    }

    public static async mergeSegments(dst: string, files: string[], folder: string = './', progressCb: progresCb = () => { }) {
        return new Promise<string>((resolve, reject) => {
            console.log(`Merging: ${files.join(',')}`);
            const ffmpeg = FFmpeg(`${join(folder, files.shift())}`);
            files.forEach(sgmnt => {
                ffmpeg.addInput(`${join(folder, sgmnt)}`);
            });
            ffmpeg
                .on('progress', progressCb)
                .on('end', function(err: Error) {
                    if (!err) {
                        return resolve(`${dst}.mp4`);
                    }
                    reject(err);
                })
                .on('error', function(err: Error) {
                    reject(err);
                })
                .mergeToFile(`${join(folder, dst)}.mp4`);
        });
    }
}